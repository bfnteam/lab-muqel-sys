package com.muqel.service;

import com.muqel.service.hellohelper.HelloHelperService;
import com.muqel.service.hellohelper.HelloHelperServiceImpl;
import org.junit.Assert;
import org.junit.Test;

public class HelloHelperServiceTest {

    @Test
    public void helloHelperTestOk() {
        HelloHelperService helloHelperService = new HelloHelperServiceImpl();
        String msgExpected = helloHelperService.helloHelper();
        Assert.assertEquals("Hello, help me", msgExpected);
    }

}

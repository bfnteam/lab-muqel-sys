package com.muqel.service;

import com.muqel.service.hello.HelloService;
import com.muqel.service.hello.HelloServiceImpl;
import com.muqel.service.hellohelper.HelloHelperService;
import org.junit.Assert;
import org.junit.Test;

import static org.easymock.EasyMock.*;

public class HelloServiceTest {

    @Test
    public void helloTestOk() {
        HelloHelperService helloHelperServiceMock = createMock(HelloHelperService.class);
        HelloService helloService = new HelloServiceImpl(helloHelperServiceMock);

        String messageExpected = "a message";
        expect(helloHelperServiceMock.helloHelper()).andReturn(messageExpected).times(1);

        replay(helloHelperServiceMock);

        String messageActual = helloService.hello();

        Assert.assertEquals(messageExpected, messageActual);

        verify(helloHelperServiceMock);
    }


}

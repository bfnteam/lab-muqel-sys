package com.muqel.service.todolist;

import com.muqel.todo.TodoEntity;

import java.util.List;

public interface ITodoListService {
    List<TodoEntity> getAllTodo();

    List<TodoEntity> getListTodoByContent(String content);

    void generateTodoList();
}

package com.muqel.service.todolist;

import com.muqel.repository.todolist.ITodoListRepository;
import com.muqel.todo.TodoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class TodoListServiceImpl implements ITodoListService{

    @Autowired
    private ITodoListRepository todoListRepository;

    @Override
    public List<TodoEntity> getListTodoByContent(String content){
        List<TodoEntity> listTodo = StreamSupport.stream(todoListRepository.findByContentLike(content).spliterator(),false).collect(Collectors.toList());
        return listTodo;
    }

    @Override
    public List<TodoEntity> getAllTodo(){
        List<TodoEntity> listTodo = StreamSupport.stream(todoListRepository.findAll().spliterator(),false).collect(Collectors.toList());
        return listTodo;
    }

    @Override
    public void generateTodoList(){
        generateData(10);
    }

    private void generateData(int nbrTodoToGenerate){

        List<TodoEntity> todoEntityList = new ArrayList<TodoEntity>();
        for (int i=1;i<=nbrTodoToGenerate;i++){
            todoEntityList.add(new TodoEntity("todo"+i));
        }

        todoListRepository.save(todoEntityList);
    }
}

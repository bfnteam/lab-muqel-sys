package com.muqel.service.hello;

import com.muqel.service.hellohelper.HelloHelperService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log
public class HelloServiceImpl implements HelloService {

    private final HelloHelperService helloHelperService;

    @Autowired
    public HelloServiceImpl(HelloHelperService helloHelperService) {
        this.helloHelperService = helloHelperService;
    }

    @Override
    public String hello() {
        log.info("hello called");
        return helloHelperService.helloHelper();
    }
}

package com.muqel.service.hellohelper;

import lombok.extern.java.Log;
import org.springframework.stereotype.Component;

@Component
@Log
public class HelloHelperServiceImpl implements HelloHelperService {

    public HelloHelperServiceImpl() {
    }

    @Override
    public String helloHelper() {
        log.info("helloHelper called");
        return "Hello, help me";
    }
}

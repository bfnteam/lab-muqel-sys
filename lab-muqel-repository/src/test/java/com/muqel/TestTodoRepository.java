package com.muqel;

import com.muqel.repository.todolist.ITodoListRepository;
import com.muqel.todo.TodoEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class TestTodoRepository {

    @Autowired
    ITodoListRepository todoListRepository;



    @Test
    public void findAll(){
        generateData(5);
        List<TodoEntity> generatedList = StreamSupport.stream(todoListRepository.findAll().spliterator(),false).collect(Collectors.toList());
        assert generatedList.size() == 5;
    }

    @Test
    public void findByContentLikeTest(){
        generateData(5);
        List<TodoEntity> generatedList = StreamSupport.stream(todoListRepository.findByContentLike("1").spliterator(),false).collect(Collectors.toList());
        assert generatedList.get(0).getId() == 1;
    }

    public void generateData(int nbrTodoToGenerate){

        List<TodoEntity> todoEntityList = new ArrayList<TodoEntity>();
        for (int i=1;i<=nbrTodoToGenerate;i++){
            todoEntityList.add(new TodoEntity("todo"+i));
        }

        todoListRepository.save(todoEntityList);
    }
}

package com.muqel;

import com.muqel.repository.todolist.ITodoListRepository;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackageClasses = { ITodoListRepository.class })
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.muqel.todo"})
public class TestConfiguration {
}

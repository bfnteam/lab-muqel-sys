package com.muqel.repository.todolist;


import com.muqel.todo.TodoEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITodoListRepository extends CrudRepository<TodoEntity, Long> {

    @Query("select todo from TodoEntity todo where todo.content like %?1%")
    List<TodoEntity> findByContentLike(String content);

}

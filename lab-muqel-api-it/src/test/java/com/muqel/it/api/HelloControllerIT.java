package com.muqel.it.api;

import com.muqel.AppMuqelApi;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static io.restassured.RestAssured.given;

@SpringBootTest(classes = AppMuqelApi.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@ActiveProfiles("integration-api")
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HelloControllerIT {

    @LocalServerPort
    private int port;

    @BeforeClass
    public static void setupClass() {
        RestAssured.baseURI = "http://localhost";
    }

    @Before
    public void setup() {
        RestAssured.port = port;
    }

    @Test
    public void helloTestIT_1_RestTemplate() {
        String expected = "Hello, help me 1";
        String actual = new RestTemplate().getForObject("http://localhost:" + port + "/hello", String.class);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void helloTestIT_2_RestAssured() {
        given()
            .when()
                .get("hello")
            .then()
                .statusCode(Matchers.is(200))
                .body(Matchers.equalTo("Hello, help me 2"));
    }
}

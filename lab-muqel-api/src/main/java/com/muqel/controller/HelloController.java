package com.muqel.controller;

import com.muqel.service.hello.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class HelloController {
    private final HelloService helloService;
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    public HelloController(HelloService helloService) {
        this.helloService = helloService;
    }

    @RequestMapping("hello")
    public String hello() {
        String msg = this.helloService.hello();
        return msg + " " + counter.incrementAndGet();
    }

}

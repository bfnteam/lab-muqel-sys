package com.muqel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class AppMuqelApi {
    public static void main( String[] args ) {
        SpringApplication.run(AppMuqelApi.class, args);
    }
}

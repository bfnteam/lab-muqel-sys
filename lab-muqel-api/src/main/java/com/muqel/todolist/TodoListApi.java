package com.muqel.todolist;

import com.muqel.service.todolist.ITodoListService;
import com.muqel.todo.TodoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TodoListApi {
    @Autowired
    ITodoListService todoListService;

    @GetMapping("/listTodo")
    public List<TodoEntity> listTodoList(){
        return todoListService.getAllTodo();
    }

    @GetMapping("/generateData")
    public void generateData(){
        todoListService.generateTodoList();
    }
}
